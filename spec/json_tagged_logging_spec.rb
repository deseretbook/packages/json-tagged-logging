RSpec.describe JsonTaggedLogging do
  subject(:logger) { JsonTaggedLogging.new(Logger.new(transport)) }
  let(:transport) { StringIO.new }
  
  let(:message) { 'Test' }

  def test_keys(message)
    expect(message).to have_key('message')
    expect(message).to have_key('timestamp')
    expect(message).to have_key('level')
    expect(message).to have_key('app')
  end

  it "has a version number" do
    expect(JsonTaggedLogging::VERSION).not_to be nil
  end

  it 'must output the message as json' do
    logger.info(message)

    transport.rewind

    expect(transport.read).to be_a_json_formatted_string
  end

  it 'must have the proper keys' do
    logger.info(message)

    transport.rewind

    message = Oj.load(transport.read)

    test_keys(message)
  end

  context 'when the message is a hash' do
    let(:message) { { message: 'test', data: { a: 1, b: 2 } } }

    it 'must merge the hash with the rest of the log' do
      logger.info message 
      
      transport.rewind

      message = Oj.load(transport.read)

      expect(message).to include('data' => { 'a' => 1, 'b' => 2 })
      expect(message['message']).to eql 'test'
      test_keys(message)
    end
  end

  context 'when the message is an exception' do
    let(:message) { StandardError.new('oops') }

    it 'must include an error object in the log' do
      logger.info message
      transport.rewind

      message = Oj.load(transport.read)

      expect(message).to have_key('error')
      expect(message).to include('message' => 'oops', 'error' => { 'name' => 'StandardError', 'message' => 'oops', 'trace' => anything })
      test_keys(message)
    end
  end

  describe 'tagged logging' do
    it 'must merge tags with log data' do
      logger.tagged(user: 1) do
        logger.info message
      end

      transport.rewind

      message = Oj.load(transport.read)

      expect(message).to include('user' => 1)
      test_keys(message)
    end

    it 'must give precedence to tag keys' do
      logger.tagged(user: 1) do
        logger.info user: 2
      end

      transport.rewind

      message = Oj.load(transport.read)
      expect(message['user']).to eql 1
      test_keys(message)
    end
  end
end
