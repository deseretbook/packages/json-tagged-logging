RSpec::Matchers.define :be_a_json_formatted_string do |expected|
  match do |actual|
    begin
      Oj.load(actual)
      true
    rescue Oj::ParserError
      false
    end
  end
end

RSpec::Matchers.alias_matcher :a_json_formattted_string, :be_a_json_formatted_string
