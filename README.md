# JsonTaggedLogging

This gem is for the purpose of serializing your logs as JSON. This can be valuable if you're shipping logs
and need to easily search data.

This logger is very similar to [ActiveSupport::TaggedLogging](https://api.rubyonrails.org/classes/ActiveSupport/TaggedLogging.html). The
difference rather that tags being strings, your tags can be key/value pairs.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'json_tagged_logging'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install json_tagged_logging

## Usage

**Decorate your logger**

```ruby
logger = Logger.new(STDOUT)
logger = JsonTaggedLogging.new(logger)

logger.info 'This is a log'
#=> {"level":"INFO","timestamp":"2020-05-29T21:23:43.990Z","app":null,"message":"This is a log"}
```

**Tag your logs**

```ruby
logger.tagged('request.id': 'abc123') do
  logger.info 'Some action occurred'
end
#=> {"level":"INFO","timestamp":"2020-05-29T21:27:26.810Z","app":null,"message":"Some action occurred","request.id":"abc123"}
```

**Other usage**

```ruby
# Pass in a hash
logger.info data: { x: 1, y: 2 }
#=> {"level":"INFO","timestamp":"2020-05-29T21:29:02.324Z","app":null,"data":{"x":1,"y":2},"message":null}

# Pass in a hash and include a message
logger.info message: 'my message', data: { x: 1, y: 2 }
#=> {"level":"INFO","timestamp":"2020-05-29T21:30:13.106Z","app":null,"data":{"x":1,"y":2},"message":"my message"}

# Pass in an Exception
logger.error(StandardError.new('oops'))
#=> {"level":"ERROR","timestamp":"2020-05-29T21:32:15.488Z","app":null,"message":"oops","error":{"name":"StandardError","message":"oops","trace":null}}
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/deseretbook/packages/json-tagged-logging. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/deseretbook/packages/json-tagged-logging/-/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the JsonTaggedLogging project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/deseretbook/packages/json-tagged-logging/-/blob/master/CODE_OF_CONDUCT.md).
