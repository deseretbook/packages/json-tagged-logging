##
# Formats logs as JSON
#
module JsonTaggedLogging
  module Formatter # :nodoc:
    include ActiveSupport::TaggedLogging::Formatter

    RESERVED_FIELDS = %i[level timestamp app message trace]

    def call(severity, timestamp, progname, msg)
      tags = tags_hash.except(*RESERVED_FIELDS)

      log = {
        level: severity,
        timestamp: timestamp.utc,
        app: progname
      }

      log.merge!(serialize_message(msg))
      log.merge!(tags)

      Oj.dump(log, mode: :compat) + "\n"
    end

    def tags_hash
      tags = current_tags.inject do |memo, value|
        value.is_a?(Hash) ? memo.merge(value) : memo
      end

      tags.is_a?(Hash) ? tags : {}
    end

    private

    def serialize_message(msg)
      serialized = {}

      if msg.is_a?(Hash)
        serialized.merge! msg.except(*RESERVED_FIELDS)
        serialized[:message] = msg[:message] || msg['message']
      elsif msg.is_a?(Exception)
        serialized[:message] = msg.message
        serialized[:error] = {
          name: msg.class.name,
          message: msg.message,
          trace: msg.backtrace
        }
      else
        serialized[:message] = msg
      end

      serialized
    end
  end
end
