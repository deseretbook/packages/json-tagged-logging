require 'active_support/core_ext'
require 'active_support/json'
require 'active_support/tagged_logging'
require 'oj'

require 'json_tagged_logging/formatter'
require 'json_tagged_logging/version'

##
# Wraps any standard Logger object to provide tagging capabilities.
#
# This implementation is wideley taken from ActiveSupport::TaggedLogging with
# some modifications
#
# @see https://github.com/rails/rails/blob/master/activesupport/lib/active_support/tagged_logging.rb
#
# @example Creating Logs
#   logger = JsonTaggedLogging.new(Logger.new(STDOUT))
#
#   logger.tagged(channel: 'api') { logger.info 'Stuff' }
#   #=> { "level": "INFO", "message": "Stuff", "channel": "api" }
#
#   logger.tagged(channel: 'api', user: 1) { logger.info 'Stuff' }
#   #=> { "level": "INFO", "message": "Stuff", "channel": "api", "user": 1 }
#
#   logger.tagged(channel: 'api') { logger.tagged(user: 1) { logger.info 'Stuff' } }
#   #=> { "level": "INFO", "message": "Stuff", "channel": "api", "user": 1 }
#
module JsonTaggedLogging
  def self.new(logger)
    logger = logger.dup

    if logger.formatter
      logger.formatter = logger.formatter.dup
    else
      # Ensure we set a default formatter so we aren't extending nil!
      logger.formatter = ActiveSupport::Logger::SimpleFormatter.new
    end

    logger.formatter.extend JsonTaggedLogging::Formatter
    logger.extend(self)
  end

  delegate :push_tags, :pop_tags, :clear_tags!, to: :formatter

  def tagged(*tags)
    formatter.tagged(*tags) { yield self }
  end

  def flush
    clear_tags!
    super if defined?(super)
  end
end

